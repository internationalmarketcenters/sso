module.exports = function() {
  'use strict';

  const glob     = require('glob');
  const globOpts = {cwd: __dirname + '/../'};
  const base     = __dirname + '/../';
  const files    =
    [`${base}AMC.SSO.js`]
    .concat(glob.sync('**/*.js', globOpts).filter(function(script) {
      return !script.match(/^node_modules/) &&
             !script.match(/^AMC.SSO.js/) &&
             !script.match(/^build/) &&
             !script.match(/^grunt/i) &&
             !script.match(/^demo/);
    }));

  console.dir(files);
  return files;
};


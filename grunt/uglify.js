module.exports = function(grunt, scripts, dest) {
  'use strict';

  const uglify = {
    dist: {
      files: [{
        src:  scripts,
        dest: dest
      }]
    }
  };

  grunt.loadNpmTasks('grunt-contrib-uglify');

  return uglify;
};


module.exports = function(grunt) {
  'use strict';

  const scripts   = require('./grunt/scriptGarner')();
  const buildFile = __dirname + '/build/AMC.SSO.min.js';

  grunt.initConfig({
    jshint: require('./grunt/jshint')(grunt, scripts),
    uglify: require('./grunt/uglify')(grunt, buildFile, buildFile),
    concat: require('./grunt/concat')(grunt, scripts, buildFile),
    babel:  require('./grunt/babel')(grunt, buildFile)
  });

  grunt.registerTask('default', ['jshint', 'concat', 'babel', 'uglify']);
  grunt.registerTask('build',  ['jshint', 'concat']);
};


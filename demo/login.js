(function(angular) {
  'use strict';

  angular.module('AMC.SSO.Demo')
    .component('login', {
      templateUrl : 'login.html',
      controller  : loginCtrl,
      controllerAs: 'vm'
    });

  loginCtrl.$inject = [
    '$rootScope',
    'amc_browserAuthStrategy',
    'amc_LoginSvc',
    'amc_LogoutSvc',
    'amc_RefreshTokenSvc',
    'amc_authCookieSvc'
  ];

  function loginCtrl($rootScope, authStrat, LoginSvc, LogoutSvc, RefreshTokenSvc, authCookieSvc) {
    const vm         = this;

    const loginSvc   = new LoginSvc(authStrat);
    const refreshSvc = new RefreshTokenSvc(authStrat);
    const logoutSvc  = new LogoutSvc();

    vm.login           = loginSvc.login.bind(loginSvc);
    vm.isLoggedIn      = loginSvc.isLoggedIn.bind(loginSvc);
    vm.logout          = logoutSvc.logout.bind(logoutSvc);
    vm.refresh         = refresh;
    vm.getAuthToken    = authCookieSvc.getAuthToken.bind(authCookieSvc);
    vm.getTokenPayload = authCookieSvc.getTokenPayload.bind(authCookieSvc);
    vm.timeToExp       = authCookieSvc.getTimeUntilExpiration();

    // Try to refresh the token.  The response is resolved if the token
    // is renewed, otherwise it is rejected.
    function refresh() {
      refreshSvc
        .renewAuthToken()
        .catch(function(err) {
          console.error('Cannot renew auth token.  The user does not have a valid session with the auth server.');
        });
    }

    $rootScope.$on('AMC.SSO.LOGIN_EXPIRATION_POLL', function(e, exp) {
      vm.timeToExp = exp;
    });
  }
})(window.angular);


(function(angular) {
  'use strict';

  angular.module('AMC.SSO.Demo', ['AMC.SSO'])
    .config(['amc_authConfigProvider', configAuth]);
  
  /**
   * Configure the AMC.SSO app.
   */
  function configAuth(authConfigProvider) {
    // The client_id of this app, which is registered with WEM.
    authConfigProvider.setClientID('AMC.SSO.Demo');

    // The Auth endpoint.  May differ between environments (dev/uat/prod).
    authConfigProvider.setAuthEndpoint('https://dev.lightfair.com');

    // Where to send users back to after a login attempt.
    // Note the custom scheme, which is used to launch the cordova app.
    authConfigProvider.setLoginRedirectUri('http://localhost:3000/demo/');
    //authConfigProvider.setLoginRedirectUri('amcrefresh://authorize');

    // Where silent authentication attempts redirect to (an iframe).  This is
    // ignored for mobile user agents.
    authConfigProvider.setRefreshRedirectUri('http://localhost:3000/demo/auth-frame.html');

    // Optional:  Custom name for the auth token cookie.
    authConfigProvider.setAuthTokenCookieName('__AMC_SSO_AUTHTOKEN');
  }
})(window.angular);



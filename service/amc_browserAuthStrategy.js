(function(angular) {
  'use strict';

  angular.module('AMC.SSO')
    .factory('amc_browserAuthStrategy', browserAuthStrategyProducer);

  browserAuthStrategyProducer.$inject = [
    '$window',
    '$location',
    '$q',
    'amc_authConfig',
    'amc_AuthSvc'
  ];

  function browserAuthStrategyProducer($window, $location, $q, authConfig, AuthSvc) {
    class BrowserAuthStrategy {
      /**
       * Initialize the route change listener.
       * @param {AuthSvc} [authSvc=AuthSvc()] - An AuthSvc instance.  Defaults to
       * new AuthSvc().  Used for building the auth url.
       */
      constructor(authSvc) {
        this.authSvc = authSvc || new AuthSvc();

        if ($window.location.href.indexOf(authConfig.loginRedirectUri) === 0 && $location.hash()) {
          this._handleAuthResponse();
        }
      }

      /**
       * Go to the auth endpoint.
       */
      goAuth(authURL) {
        $window.location.href = authURL;
      }

      /**
       * Refresh the token client side in an iframe.  Assumes that the token has
       * already been renewed on the auth server side.
       */
      renewClientAuthToken() {
        const self = this;

        // In order to refresh the token client side, the user agent must be
        // redirected to the auth endpoint, which is done silently in a hidden
        // iframe.
        const iframe = $window.document.createElement('iframe');
        const defer  = $q.defer();

        iframe.style.display = 'none';
        iframe.onload        = thunk_handleAuthResponse;
        iframe.src           = this.authSvc.buildAuthURL(authConfig.refreshRedirectUri);
        $window.document.body.appendChild(iframe);

        return defer.promise;

        // Fires when the iframe loads (which is after the redirect) and
        // handles the auth response.
        function thunk_handleAuthResponse() {
          // Handle the response by verifying the nonce and storing the
          // auth token.
          self.authSvc.handleAuthResponse(iframe.contentWindow.location.href);

          // Resolve the promise so the caller can proceed.
          defer.resolve();

          // Get rid of the hidden iframe.
          iframe.remove();
        }
      }

      /**
       * Private helper method that fires when a URL containing this app's 
       * redirect_uri is opened.
       */
      _handleAuthResponse() {
        // Only the fragment is needed, and the fragment is expected to be included.
        this.authSvc.handleAuthResponse('#' + $location.hash());

        // Clear the hash--it's no longer needed.
        $location.hash('');
      }
    }

    return new BrowserAuthStrategy();
  }
})(window.angular);


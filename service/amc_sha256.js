(function() {
  'use strict';

  angular.module('AMC.SSO')
    .factory('amc_sha256', ['$window', sha256Producer]);

  function sha256Producer($window) {
    if (!$window.sha256) {
      throw new Error('sha256 is required to be on window.  Install it by running: bower install --save js-sha256');
    }

    return $window.sha256;
  }
})();


(function(angular) {
  'use strict';

  angular.module('AMC.SSO')
    .factory('amc_RefreshTokenSvc', refreshTokenSvcProducer);

  refreshTokenSvcProducer.$inject = [
    '$http',
    '$rootScope',
    '$sce',
    'amc_authConfig'
  ];

  function refreshTokenSvcProducer($http, $rootScope, $sce, authConfig) {
    const REFRESH_ENDPOINT = $sce.trustAsResourceUrl(authConfig.authEndpoint + '/api/Login/Refresh');

    class RefreshTokenSvc {
      /**
       * Ctor.
       * @param {AuthStrategy} authStrategy - In AuthStrategy instance that is
       * suitable for the user agent (browser or mobile).
       */
      constructor(authStrat) {
        this.authStrat = authStrat;
      }

      /**
       * Renew the user's auth if possible.
       */
      renewAuthToken() {
        const self = this;

        // Hit the refresh enpoint, which will renew the auth token on the
        // auth server (not in the client).  The hit either returns a 401,
        // which means that the token could not be renewed, or a 200.
        return $http
          .jsonp(REFRESH_ENDPOINT)
          .then(function() {
            // The token was refreshed on the auth server side.  Refresh client
            // side using the auth strategy.
            return self.authStrat.renewClientAuthToken();
          })
          .then(function() {
            $rootScope.$broadcast('AMC.SSO.AUTH_RENEWED');
          });
      }
    }

    return RefreshTokenSvc;
  }
})(window.angular);


(function(angular) {
  'use strict';

  angular.module('AMC.SSO')
    .factory('amc_AuthSvc', [
      '$rootScope',
      'bgi_RandomSvc',
      'bgi_storageSvc',
      'amc_authConfig',
      'amc_authCookieSvc',
      'amc_tokenizer',
      'amc_sha256',
      authSvcProducer
    ]);

  function authSvcProducer($rootScope, RandomSvc, storageSvc, authConfig,
    authCookieSvc, tokenizer, sha256) {

    const NONCE_NAME = 'AMC.SSO-nonce';
    const STATE_NAME = 'AMC.SSO-state';

    class AuthSvc {
      /**
       * Build the auth URL.
       */
      buildAuthURL(redirectUri) {
        const params  = [];
        const nonce   = RandomSvc.generateRandomString(30);
        const state   = RandomSvc.generateRandomString(30);
        let   authURL = authConfig.authEndpoint + '/api/OpenID/Authorization?';

        // "state" and "nonce" get saved in local storage so that they can be
        // verified when the auth servers sends the user back.
        //
        // The state is to pick up where the user left of (if needed).
        //
        // The nonce is used to prevent replay attacks.  If a redirect back
        // from the auth server is comprimised, an attacker would not be able
        // to blindly use it without the clear-text nonce (it's sent to the
        // auth server hashed).
        storageSvc.setObject(NONCE_NAME, nonce);
        storageSvc.setObject(STATE_NAME, state);

        params.push('scope=openid');
        params.push('response_type=id_token');
        params.push('client_id='    + encodeURIComponent(authConfig.clientID));
        params.push('redirect_uri=' + encodeURIComponent(redirectUri));
        params.push('nonce='        + encodeURIComponent(sha256(nonce)));
        params.push('state='        + encodeURIComponent(state));

        authURL += params.join('&');

        return authURL;
      }

      /**
       * Handle an auth response from the server.
       */
      handleAuthResponse(url) {
        // Put all the hash parameters in an object.
        const hashParams = url
          .split('#')[1]
          .split('&')
          .reduce(function(prev, keyVal) {
            keyVal          = keyVal.split('='); 
            prev[keyVal[0]] = keyVal[1];

            return prev;
          }, {});

        if (hashParams.error) {
          $rootScope.$broadcast('AMC.SSO.LOGIN_ERROR', new Error(hashParams.error));
        }
        else {
          const state       = hashParams.state;
          const idToken     = hashParams.id_token;
          //const accessToken = hashParams.token;
          const localState  = storageSvc.getObject(STATE_NAME);
          const idPayload   = tokenizer.decode(idToken);
          const nonce       = idPayload.nonce;
          const localNonce  = storageSvc.getObject(NONCE_NAME);

          // Remove locally stored state information.
          storageSvc.removeObject(STATE_NAME);
          storageSvc.removeObject(NONCE_NAME);

          // Verify that the state matches up.
          if (!localState || state !== localState) {
            $rootScope.$broadcast('AMC.SSO.LOGIN_FAILURE', new Error('Invalid state.'));
            return;
          }

          // Verify the nonce matches up (replay/cut-and-paste URL attack
          // prevention).
          if (!localNonce || nonce !== sha256(localNonce)) {
            $rootScope.$broadcast('AMC.SSO.LOGIN_FAILURE', new Error('Invalid nonce.'));
            return;
          }

          // Store the token in a cookie.
          authCookieSvc.setAuthToken(idToken);

          // Set up the login.
          $rootScope.$broadcast('AMC.SSO.LOGIN_SUCCESS', idPayload);
        }
      }
    }

    return AuthSvc;
  }
})(window.angular);


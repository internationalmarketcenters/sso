(function(angular) {
  'use strict';

  angular.module('AMC.SSO')
    .factory('amc_tokenizer', ['amc_jwtDecode', tokenizerProducer]);

  function tokenizerProducer(jwtDecode) {
    /**
     * Constructor
     */
    class Tokenizer {
      /**
       * Decode a token.
       * @param jwt The token to decode.
       */
      decode(jwt) {
        if (!jwt) {
          throw new Error('No auth token supplied.');
        }

        try {
          return jwtDecode(jwt);
        }
        catch (e) { // jshint ignore:line
          throw new Error('Failed to decode token.');
        }
      }

      /**
       * Get the expiration as a date.
       * @param jwt The token to decode.
       */
      getExpiration(jwt) {
        return new Date(this.decode(jwt).exp * 1000);
      }

      /**
       * Check if the token is expired.
       * @param jwt The token to decode.
       */
      isExpired(jwt) {
        return this.getExpiration(jwt) < new Date();
      }
    }

    return new Tokenizer();
  }
})(window.angular);

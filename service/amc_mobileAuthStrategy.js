(function(angular) {
  'use strict';

  angular.module('AMC.SSO')
    .factory('amc_mobileAuthStrategy', mobileAuthStrategyProducer);

  mobileAuthStrategyProducer.$inject = [
    '$window',
    '$timeout',
    '$q',
    'amc_authConfig',
    'amc_AuthSvc'
  ];

  function mobileAuthStrategyProducer($window, $timeout, $q, authConfig, AuthSvc) {
    class MobileAuthStrategy {
      /**
       * Set the global listener.
       * @see https://github.com/EddyVerbruggen/Custom-URL-scheme
       * @param {AuthSvc} [authSvc=AuthSvc()] - An AuthSvc instance.  Defaults to
       * new AuthSvc().  Used for building the auth url.
       */
      constructor(authSvc) {
        this.authSvc          = authSvc || new AuthSvc();
        this._wndRef          = null;
        this._defer           = null;
        $window.handleOpenURL = this._handleOpenURL.bind(this);
      }

      /**
       * Open the auth url using the inAppBrowser plugin.
       */
      goAuth(authURL) {
        this._defer  = $q.defer();
        this._wndRef = $window.cordova.InAppBrowser.open(authURL, '_blank', 'location=no,toolbar=no');

        return this._defer.promise;
      }

      /**
       * Refresh the token client side.  Assumes that the token has already been
       * refreshed on the auth server side.
       */
      renewClientAuthToken() {
        const authURL = this.authSvc.buildAuthURL(authConfig.loginRedirectUri);

        this._defer  = $q.defer();
        this._wndRef = $window.cordova.InAppBrowser.open(authURL, '_blank', 'location=no,toolbar=no,hidden=yes');

        return this._defer.promise;
      }

      /**
       * Private helper method that fires when a URL containing this app's custom
       * URI scheme is opened.
       */
      _handleOpenURL(url) {
        const self = this;

        $timeout(function() {
          if (self._wndRef) {
            self._wndRef.close();
            self._wndRef = null;
            self._defer.resolve();

            if (url.indexOf(authConfig.loginRedirectUri) === 0) {
              self.authSvc.handleAuthResponse(url);
            }
          }
        }, 0);
      }
    }

    return new MobileAuthStrategy();
  }
})(window.angular);


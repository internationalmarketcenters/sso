(function(angular) {
  'use strict';

  angular.module('AMC.SSO')
    .provider('amc_authConfig', [AuthSvcProvider]);

  function AuthSvcProvider() {
    const vm = this;

    let clientID, authEndpoint, loginRedirectUri, refreshRedirectUri;
    let authTokenCookieName = '__AMC_SSO_AUTHTOKEN';

    // Setters for configuration.
    vm.setClientID            = function(ID)       { clientID            = ID;       };
    vm.setAuthEndpoint        = function(endpoint) { authEndpoint        = endpoint; };
    vm.setLoginRedirectUri    = function(uri)      { loginRedirectUri    = uri;      };
    vm.setRefreshRedirectUri  = function(uri)      { refreshRedirectUri  = uri;      };
    vm.setAuthTokenCookieName = function(name)     { authTokenCookieName = name;     };

    vm.$get = [authSvcProducer];

    function authSvcProducer() {
      // Export a config object.
      return {
        clientID:            clientID,
        authEndpoint:        authEndpoint,
        loginRedirectUri:    loginRedirectUri,
        refreshRedirectUri:  refreshRedirectUri,
        authTokenCookieName: authTokenCookieName
      };
    }
  }

})(window.angular);


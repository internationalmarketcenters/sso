(function(angular) {
  'use strict';

  angular.module('AMC.SSO')
    .factory('amc_authCookieSvc', authCookieSvcProducer);

  authCookieSvcProducer.$inject = [
    '$cookies',
    '$interval',
    '$rootScope',
    'amc_tokenizer',
    'amc_authConfig'
  ];

  function authCookieSvcProducer($cookies, $interval, $rootScope, tokenizer, config) {
    const CLOCK_SKEW_DELTA  = 300000; // Five minutes, milliseconds
    const EXP_POLL_INTERVAL = 60000;  // One minute, milliseconds

    /**
     * Constructor
     */
    class AuthCookieSvc {
      constructor() {
        this._expInterval = null;
      }

      /**
       * Set the auth token cookie.
       */
      setAuthToken(token) {
        const payload = tokenizer.decode(token);
        const expires = new Date(payload.exp - CLOCK_SKEW_DELTA);

        $cookies.put(config.authTokenCookieName, token, {expires: expires});

        // Poll until the cookie expires or the user logs out.
        this._doPoll();
        this._expInterval = $interval(this._doPoll.bind(this), EXP_POLL_INTERVAL);
      }

      /**
       * Clear the auth token cookie.
       */
      destroyAuthToken() {
        // Cancel the poll.
        if (this._expInterval) {
          $interval.cancel(this._expInterval);
        }

        $cookies.remove(config.authTokenCookieName);
      }

      /**
       * Get the id token from cookie storage.
       */
      getAuthToken() {
        return $cookies.get(config.authTokenCookieName);
      }

      /**
       * Get the payload of the id token.
       */
      getTokenPayload() {
        const token = this.getAuthToken();

        return token ? tokenizer.decode(token) : null;
      }

      /**
       * Get the number of milliseconds until the token expires.
       */
      getTimeUntilExpiration() {
        let tokenPayload = this.getTokenPayload(),
            untilExpires = 0;

        if (tokenPayload) {
          untilExpires = tokenPayload.exp - CLOCK_SKEW_DELTA - new Date().getTime();

          if (untilExpires < 0) {
            untilExpires = 0;
          }
        }

        return untilExpires;
      }

      /**
       * Poll for the logout expiration time.
       * @private
       */
      _doPoll() {
        const expiresIn = this.getTimeUntilExpiration();

        // The user can watch poll events (maybe they want to show the user
        // a "no activity for x minutes" screen, or refresh their token.
        $rootScope.$broadcast('AMC.SSO.LOGIN_EXPIRATION_POLL', expiresIn);

        // When the session expires, log out.
        if (expiresIn === 0) {
          $rootScope.$broadcast('AMC.SSO.LOGOUT');
          this.destroyAuthToken();
        }
      }
    }

    return new AuthCookieSvc();
  }
})(window.angular);


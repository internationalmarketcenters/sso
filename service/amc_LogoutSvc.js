(function(angular) {
  'use strict';

  angular.module('AMC.SSO')
    .factory('amc_LogoutSvc', logoutSvcProducer);

  logoutSvcProducer.$inject = [
    '$http',
    '$rootScope',
    '$sce',
    'amc_authConfig',
    'amc_authCookieSvc'
  ];

  function logoutSvcProducer($http, $rootScope, $sce, authConfig, authCookieSvc) {
    const LOGOUT_ENDPOINT = $sce.trustAsResourceUrl(authConfig.authEndpoint + '/api/Logout');

    /**
     * Ctor.
     */
    class LogoutSvc {
      /**
       * Log the user out.
       */
      logout() {
        return $http
          .jsonp(LOGOUT_ENDPOINT)
          .then(function(resp) {
            authCookieSvc.destroyAuthToken();
            $rootScope.$broadcast('AMC.SSO.LOGOUT');
            return resp;
          });
      }
    }

    return LogoutSvc;
  }
})(window.angular);

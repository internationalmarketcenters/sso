(function(angular) {
  'use strict';

  angular.module('AMC.SSO')
    .factory('amc_jwtDecode', ['$window', jwtDecodeProducer]);

  function jwtDecodeProducer($window) {
    if (!$window.jwt_decode) {
      throw new Error('jwt_decode is required to be on window.  Install it by running: bower install jwt-decode --save');
    }

    return $window.jwt_decode;
  }
})(window.angular);


(function(angular) {
  'use strict';

  angular.module('AMC.SSO')
    .factory('amc_LoginSvc', loginSvcProducer);

  loginSvcProducer.$inject = [
    'amc_authConfig',
    'amc_authCookieSvc',
    'amc_AuthSvc'
  ];

  function loginSvcProducer(authConfig, authCookieSvc, AuthSvc) {
    class LoginSvc {
      /**
       * Ctor.
       * @param {AuthStrategy} authStrategy - In AuthStrategy instance that is
       * suitable for the user agent (browser or mobile).
       * @param {AuthSvc} [authSvc=AuthSvc()] - An AuthSvc instance.  Defaults to
       * new AuthSvc().  Used for building the auth url.
       */
      constructor(authStrategy, authSvc) {
        this.authStrat = authStrategy;
        this.authSvc   = authSvc || new AuthSvc();
      }

      /**
       * Send the user through the authentication flow, which follows the OpenID
       * Connect implicit flow.
       */
      login() {
        // Send the user to the auth endpoint.
        this.authStrat.goAuth(this.authSvc.buildAuthURL(authConfig.loginRedirectUri));
      }

      /**
       * Helper to check if the user is logged in.
       */
      isLoggedIn() {
        return !!authCookieSvc.getAuthToken();
      }
    }

    return LoginSvc;
  }
})(window.angular);


(function(angular) {
  'use strict';

  angular.module('AMC.SSO', ['ngCookies', 'ngSanitize', 'bgi']);
})(window.angular);

